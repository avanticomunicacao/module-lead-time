<?php
namespace Avanti\LeadTime\Plugin;

use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Quote\Model\Quote\Address\Total\Shipping;
use Magento\Quote\Model\Quote\TotalsCollector;

class AddLeadTimeEstimatedShipping
{
    protected $productRepository;
    protected $totalsCollector;

    public function __construct(
        TotalsCollector $totalsCollector,
        ProductRepositoryInterface $productRepository)
    {
        $this->totalsCollector = $totalsCollector;
        $this->productRepository = $productRepository;
    }


    public function afterCollect(Shipping $subject, $result, $quote, $shippingAssignment, $total)
    {
        $items  = $quote->getItems();

        if ($items) {
            $biggestLeadTime = $this->getBiggestLeadTime($items);

            if ($biggestLeadTime != null && $biggestLeadTime > 0) {
                $shipping = $quote->getShippingAddress();
                $shipping->setCollectShippingRates(true);

                $rates = $shipping->collectShippingRates()->getAllShippingRates();

                if (count($rates)) {
                    foreach ($rates as $k => $rate) {
                        if ($rate->getCode() != 'flatrate_flatrate') {
                            try {
                                $methodTitle = explode(" ", $rate->getMethodTitle());
                                foreach ($methodTitle as $key => $value) {
                                    if (is_numeric($value)) {
                                        $positionDays = $key;
                                        continue;
                                    }
                                }
                                $days = intval($methodTitle[$positionDays]);
                                $days = $days + intval($biggestLeadTime);

                                $nameMethod = null;
                                foreach ($methodTitle as $key => $value) {
                                    if ($key == $positionDays) {
                                        $nameMethod .= $days . " ";
                                    } else {
                                        $nameMethod .= $value . " ";
                                    }
                                }

                                $rate->setMethodTitle($nameMethod);
                            } catch (\Exception $e) {
                                continue;
                            }
                        }
                    }
                }
            }
        }

        return $result;
    }


    /**
     * @param array $items
     * @return int
     */
    public function getBiggestLeadTime(array $items)
    {
        $leadTimeAtual = 0;
        foreach ($items as $item) {
            $product = $this->productRepository->getById($item->getProductId());
            if ($item->getProductType() == 'configurable') {
                $child = $item->getChildren()[0];
                $product = $this->productRepository->getById($child->getProductId());
            }
            $leadTimeProduct = intval($product->getLeadTime());
            if($leadTimeProduct > $leadTimeAtual) {
                $leadTimeAtual = $leadTimeProduct;
            }
        }

        return  $leadTimeAtual;
    }
}
